import gab.opencv.*;
import processing.video.*;
import java.awt.*;

import processing.serial.*;
Serial myPort;  // Create object from Serial class


Capture video;
OpenCV opencv;

int tiltMin = 20;
int tiltMax = 95;
int panMin = 20;
int panMax = 160;

void setup() {
  size(640, 240);
  video = new Capture(this, 640/2, 480/2);
  opencv = new OpenCV(this, 640/2, 480/2);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);  
  background(0);
  video.start();
  drawGUI();

  //Serial
  String portName = Serial.list()[0]; //change the 0 to a 1 or 2 etc. to match your port
  myPort = new Serial(this, portName, 115200);
}

void drawGUI() {
}

void redrawBG() {
  //Redraw bg
  stroke(0, 0, 0);
  fill(0, 0, 0);
  rect(320, 0, 640, 240);
}

//body length, holder length, angle
void drawTilt(int l, int h, float angle) {
  int x0 = 340;
  int y0 = 150;
  int offset = 90;
  double a = constrain(angle, tiltMin, tiltMax);

  strokeWeight(1);

  //For main body
  a = Math.toRadians(a-offset);
  float x = cos((float)a)*l;
  float y = sin((float)a)*l;
  line(x0, y0, x0+x, y0+y);

  //For holder
  double b = PI/2+a;
  float hx = cos((float)b)*h/2;
  float hy = sin((float)b)*h/2;
  line(x0+x, y0+y, x0+x-hx, y0+y-hy);
  line(x0+x, y0+y, x0+x+hx, y0+y+hy);

  //Text
  printText(x0, y0+30, 15, "Tilt");
}

//body length, holder length, angle
void drawPan(int l, int h, float angle) {
  int x0 = 500;
  int y0 = 150;
  int offset = 180;
  double a = constrain(angle, panMin, panMax);

  strokeWeight(1);
  //For main body
  a = Math.toRadians(a-offset);
  float x = cos((float)a)*l;
  float y = sin((float)a)*l;
  line(x0, y0, x0+x, y0+y);

  //For holder
  double b = PI/2+a;
  float hx = cos((float)b)*h/2;
  float hy = sin((float)b)*h/2;
  line(x0+x, y0+y, x0+x-hx, y0+y-hy);
  line(x0+x, y0+y, x0+x+hx, y0+y+hy);

  //Text
  printText(x0, y0+30, 15, "Pan");
}

void drawRange() {
  stroke(0, 255, 255);
  strokeWeight(1);
  //Horizontal
  line(0, 60, 320, 60);
  line(0, 180, 320, 180);
}

void printText(int x, int y, int size, String txt) {
  String s = txt;
  textSize(size);
  stroke(255, 255, 255);
  fill(255, 255, 255);
  text(s, x, y);  // Text wraps within text box
}

void draw() {
  scale(1);
  opencv.loadImage(video);

  image(video, 0, 0 );

  drawRange();

  noFill();
  stroke(0, 255, 0);
  strokeWeight(3);
  Rectangle[] faces = opencv.detect();
  //println(faces.length);  

  int i =0;
  if (faces.length>0) {
    int ptX = faces[i].x+faces[i].width/2;
    int ptY = faces[i].y+faces[i].height/2;
    int nY;  //Normalised Y to range bound
    if (ptY>60) {
      if (ptY>180) {
        nY = 180;
      } else {
        nY = ptY-60;
      }
    } else {
      nY=0;
    }
    //println(faces[i].x + "," + faces[i].y);

    rect(faces[i].x, faces[i].y, faces[i].width, faces[i].height);
    strokeWeight(5);
    point(ptX, ptY);
    String loc = Integer.toString(ptX)+","+Integer.toString(nY);
    printText(10, 20, 15, loc);
    float nX = map(ptX, 0, 320, 0, 180);
    redrawBG();
    stroke(255, 255, 255);
    myPort.write((int)nX);
    println((int)nX);
    //myPort.write(nY);
    drawTilt(50, 30, nY);
    drawPan(50, 30, nX);
  } else {
    println("0,0");
  }

  /*
  
   for (int i = 0; i < faces.length; i++) {
   int ptX = faces[i].x+faces[i].width/2;
   int ptY = faces[i].y+faces[i].height/2;
   
   println(faces[i].x + "," + faces[i].y);
   //rect(faces[i].x, faces[i].y, faces[i].width, faces[i].height);
   strokeWeight(5);
   point(ptX, ptY);
   printText(Integer.toString(ptX));
   } */
}

void captureEvent(Capture c) {
  c.read();
}