#include <Filters.h>
#include <Servo.h>
#include "config.h"

//Servo
Servo servoTilt, servoPan;

//Servo position
int posPan = 90;    // Yaw
int posTilt = 90;    // Pitch

//Servo hysteresis
int pCount = 0;
int hMax = 500;     //Maximum count before returning to  no motion state
int pState = 0;     //Previous state (-1: left, 0: no_motion, 1: right)

//IR Var
int distMax = 450;
int dist, distX, distY;
int leftVal, rightVal, upVal, downVal;

//IR Offset
int lOff = 0, rOff = 0, uOff = 0, dOff = 0;

// create one pole (RC) lowpass filter
FilterOnePole lowpassFilterL( LOWPASS, filterFrequency );
FilterOnePole lowpassFilterR( LOWPASS, filterFrequency );
FilterOnePole lowpassFilterU( LOWPASS, filterFrequency );
FilterOnePole lowpassFilterD( LOWPASS, filterFrequency );

//Time variable
bool idle = false;
unsigned long lastUpdate = 0;        //Get last time since last motion

void setup() {
  pinMode(IRLeds, OUTPUT);
  pinMode(13, OUTPUT);
  servoPan.attach(sPan);
  //servoTilt.attach(sTilt);
  Serial.begin(115200);   //Begin serial @ 115200bps
  servoTest();            // Test servos
  //setIROffset();            //Offset IR sensor error
  servoTilt.write(130);
  idle = false;
}

void loop() {
  getRawVal();
  processVal();
  //printVal();
  readServos(0);

  // send data only when you receive data:
  if (Serial.available() > 0) {
    // read the incoming byte:
    sweep(servoPan, posPan,  sPanMin, sPanMax, Serial.read(), false);
  }
  delay(1);
}

void readServos(bool debug) {
  posTilt = servoTilt.read();
  posPan = servoPan.read();
  if (debug) {
    Serial.print(posPan);
    Serial.print(",");
    Serial.println(posTilt);
  }
}

void servoTest() {
  readServos(1);
  sweep(servoTilt, posTilt, sTiltMin, sTiltMax, sTiltMax, true);    //Test Pitch
  sweep(servoPan, posPan, sPanMin, sPanMax, 90, true);       //Test Yaw
}

void sweep(Servo s, int servoPos, int sMin, int sMax, int finalPos, bool sweep) {
  if (sweep) {
    while (servoPos < sMax) {
      servoPos += 1;
      s.write(servoPos);
      delay(10);
      readServos(1);
    }
    while (servoPos > sMin) {
      servoPos -= 1;
      s.write(servoPos);
      delay(10);
      readServos(1);
    }
  }
  while (servoPos != finalPos) {
    if (servoPos < finalPos) {
      servoPos += 1;
    } else {
      servoPos -= 1;
    }
    s.write(servoPos);
    delay(10);
    readServos(1);
  }
}

void setIROffset() {
  digitalWrite(13, HIGH);
  //Get avg of j val for offset
  int j = 100;
  Serial.println("Setting offset...");
  for (int i = 0; i < j; i++) {
    getRawVal();
    lOff += leftVal;
    rOff += rightVal;
    uOff += upVal;
    dOff += downVal;
    delay(1);
  }
  lOff /= j;
  rOff /= j;
  uOff /= j;
  dOff /= j;
  Serial.println(lOff);
  Serial.println(rOff);
  Serial.println(uOff);
  Serial.println(dOff);
  digitalWrite(13, LOW);
}

void getRawVal() {
  digitalWrite(IRLeds, HIGH);
  leftVal = analogRead(IRLeft);
  rightVal = analogRead(IRRight);
  upVal = analogRead(IRUp);
  downVal = analogRead(IRDown);
}

void processVal() {
  lowpassFilterL.input(leftVal);
  lowpassFilterR.input(rightVal);
  lowpassFilterU.input(upVal);
  lowpassFilterD.input(downVal);
  leftVal = int(lowpassFilterL.output());
  rightVal = int(lowpassFilterR.output());
  upVal = int(lowpassFilterU.output());
  downVal = int(lowpassFilterD.output());

  dist = (leftVal + rightVal + upVal + downVal) / 4; // distance of object is average of reflected IR
  distX = (leftVal - rightVal) / 2;
  distY = upVal - downVal;
}

void printVal() {
  Serial.print(leftVal - lOff);
  Serial.print(",");
  Serial.print(rightVal - rOff);
  Serial.print(",");
  Serial.print(upVal - uOff);
  Serial.print(",");
  Serial.println(downVal - dOff);
  /*
    Serial.print(downVal - dOff);
    Serial.print(",");
    Serial.print(distX);
    Serial.print(",");
    Serial.println(distY);
  */
}

void moveServo( Servo s, int servoPos, int dist, int sMin, int sMax, int threshold, bool invert) {
  int tLeft = threshold;    //Left treshold
  int tRight = threshold;  //Right threshold
  int hFactor = 2;        //Hysteresis Factor
  //Implement hysteresis
  /*
    switch (pState) {
    case - 1:
      //Make it harder to go right
      tRight *= hFactor;
      break;
    case 1:
      //Make it harder to go left
      tLeft *= hFactor;
      break;
    case 0:
      //No change to threshold
      break;
    }
  */
  if (dist < -tLeft) {
    servoPos += 1;
    pState = -1;
    if (invert) {
      servoPos -= 2;
      pState = 1;
    }

  } else if (dist > tRight) {
    servoPos -= 1;
    pState = 1;
    if (invert) {
      servoPos += 2;
      pState = -1;
    }

  }
  servoPos = constrain(servoPos, sMin, sMax);
  s.write(servoPos);
  delay(5);
}

