import processing.serial.*;     // import the Processing serial library
Serial myPort;                  // The serial port
int sWidth=400, sHeight=400;
float xHairLength = 200;
//float xpos, ypos;           // Starting position of the ball
float l, r, u, d;              //Determine distance from object
int xOff=-14, yOff=0;
void setup() {
  size(400, 400, P3D);

  // List all the available serial ports
  println(Serial.list());

  // Change the 0 to the appropriate number of the serial port
  // that your microcontroller is attached to.
  myPort = new Serial(this, Serial.list()[0], 115200);


  // draw with smooth edges:
  smooth();
}

void draw() {
  background(255);
  // Draw the shape
  // ellipse((640/2)+xpos, (640/2)+ypos, 20, 20);

  //X-Axis
  ellipse(sWidth/4, sHeight/2, l, l);
  ellipse(3*sWidth/4, sHeight/2, r, r);
  //Y-Axis
  ellipse(sWidth/2, sHeight/3, u, u);
  ellipse(sWidth/2, 2*sHeight/3, d, d);

  //Avg loc
  ellipse((r-l)/300*sWidth+sWidth/2, (d-u)/300*sHeight+sHeight/2, 20, 20);


  //Draw crosshead
  stroke(0);
  line((sWidth/2)-250, sHeight/2, (sWidth/2)+250, sHeight/2);
  stroke(0);
  line(sWidth/2, (sHeight/2)-250, sWidth/2, (sHeight/2)+250);
}

// serialEvent  method is run automatically by the Processing applet
// whenever the buffer reaches the  byte value set in the bufferUntil() 
// method in the setup():

void serialEvent(Serial myPort) { 
  // read the serial buffer:
  String myString = myPort.readStringUntil('\n');
  if (myString != null) {
    // if you got any bytes other than the linefeed:
    myString = trim(myString);

    // split the string at the commas
    // and convert the sections into integers:
    int sensors[] = int(split(myString, ','));

    // print out the values you got:
    for (int sensorNum = 0; sensorNum < sensors.length; sensorNum++) {
      print("S" + sensorNum + ": " + sensors[sensorNum] + "\t");
    }
    // add a linefeed after all the sensor values are printed:
    println();

    if (sensors.length > 1) {
      //float left,right,up,down;
      r = map(sensors[0], -10, 1000, 15, 300);
      l = map(sensors[1], -10, 1000, 15, 300);   
      d = map(sensors[2], -10, 1000, 15, 300);
      u = map(sensors[3], -10, 1000, 15, 300);

      //xpos=right-left+xOff;
      //ypos=up-down+yOff;
    }
  }
}